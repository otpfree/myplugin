//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Platform.rc
//
#define IDR_MANIFEST                    1
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDR_PLATFORM                    103
#define IDR_SERVER                      104
#define IDD_OCXCALLDLG                  106
#define IDR_MAINFRAME                   128
#define IDR_PlatformTYPE                129
#define ID_MENU_OPTION                  130
#define ID_TEST_                        131
#define IDR_POPUP_TABLE                 132
#define IDR_POPUP                       132
#define IDR_POPUP_QUERY                 133
#define IDB_BITMAP2                     134
#define ID_OCXCALL                      135
#define ID_ACTIVEXMENU                  136
#define ID_ADDTOOLBAR                   142
#define ID_TABLE_SCHEMA                 32774
#define ID_TABLE_DATA                   32775
#define ID_TABLE_INSERT                 32776
#define ID_QUERY_RUN                    32777
#define ID_VIEWCONTROL                  40000
#define ID_VIEWTOOLBAR                  45000
#define ID_DYNAMENU                     50000

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        143
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           109
#endif
#endif
