// PlatformView.cpp : CPlatformView 类的实现
//

#include "stdafx.h"
#include "Platform.h"


#include "mainfrm.h"
#include "PlatformDoc.h"
#include "PlatformView.h"

//----支持COM-----------------------------
#include <objbase.h>
#include <comcat.h>
#include <initguid.h>
#include <assert.h>
#include "LmhPlugin.h" //CATID_LMHPluginCategory
const int CLSID_STRING_SIZE = 39 ;

#include <afxtempl.h>
#include <oleauto.h>

//-----------------------------------------
#include "../plugin2/Plugin2.h"
#include "../plugin2/Plugin2_i.c"
interface IPlugin;
//---------------------------

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//#include "commstruct.h"
//struct myGraph;

// CPlatformView

IMPLEMENT_DYNCREATE(CPlatformView, CScrollView)

BEGIN_MESSAGE_MAP(CPlatformView, CScrollView)
	// 标准打印命令
	ON_COMMAND(ID_FILE_PRINT, CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CScrollView::OnFilePrintPreview)
	ON_WM_RBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
END_MESSAGE_MAP()

// CPlatformView 构造/销毁
CPlatformView::CPlatformView()

{
	//-----------------------------
	m_AddMenu1=false; m_AddMenu2=false;
	m_basicMenuID=m_currentMenuID=ID_DYNAMENU;
	//初始化菜单--------------------
	m_child1Menu.DestroyMenu();
	m_child1Menu.CreatePopupMenu();
	m_child2Menu.DestroyMenu();
	m_child2Menu.CreatePopupMenu();
	//------------------------------
	m_MenuPluginList.InitMenu(ID_DYNAMENU);
	m_pServer=NULL;
	m_pDlg=NULL;
}

CPlatformView::~CPlatformView()
{
	m_child1Menu.DestroyMenu();
	m_child2Menu.DestroyMenu();
	m_MenuPluginList.ClearAll();
	m_interfaceList.ClearAll();
	if (m_pServer!=NULL)
	{	
		((IServer*)m_pServer)->Release();
		//delete m_pServer;
	}	
	m_ActiveXList.ClearAll();

}

BOOL CPlatformView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改 CREATESTRUCT cs 来修改窗口类或
	// 样式

	return CScrollView::PreCreateWindow(cs);
}

// CPlatformView 绘制
void CPlatformView::OnDraw(CDC* pDC)
{
	CPlatformDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	POSITION pos=m_interfaceList.m_interfaceList.GetHeadPosition();
	CInterfaceItem *mItem;
	while(pos!=NULL)
	{
		mItem=m_interfaceList.m_interfaceList.GetAt(pos);
		IPlugin * pPlugin;
		pPlugin=(IPlugin*)(	mItem->m_interface);
		pPlugin->ReDraw();
		m_interfaceList.m_interfaceList.GetNext(pos);
	}
		
}

void CPlatformView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();
	CSize sizeTotal;
	// TODO: 计算此视图的合计大小
	sizeTotal.cx = sizeTotal.cy = 100;
	SetScrollSizes(MM_TEXT, sizeTotal);
}


// CPlatformView 打印

BOOL CPlatformView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 默认准备
	return DoPreparePrinting(pInfo);
}

void CPlatformView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 打印前添加额外的初始化
}

void CPlatformView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 打印后添加清除过程
}


// CPlatformView 诊断

#ifdef _DEBUG
void CPlatformView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CPlatformView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}

CPlatformDoc* CPlatformView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CPlatformDoc)));
	return (CPlatformDoc*)m_pDocument;
}
#endif //_DEBUG


// CPlatformView 消息处理程序

void CPlatformView::OnRButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	//DoPopupMenu(IDR_POPUP_TABLE);
	CScrollView::OnRButtonDown(nFlags, point);
}

void CPlatformView::DoPopupMenu(UINT nMenuID)
{
	//NULL Operation!
}

void CPlatformView::OnPopupCommand(UINT nMenuID)
{
	//NULL Operation!
}


//----------------------------------
//刷新插件的菜单项
//---------------------------------
void CPlatformView::RefreshMenu(CMenu &childMenu, char* menuName)
{
	//在主菜单上加上完全动态菜单
	CMenu* pTopMenu = AfxGetMainWnd()->GetMenu();
	int iPos = pTopMenu->GetMenuItemCount()-1;
	pTopMenu->InsertMenu(iPos,MF_POPUP|MF_BYPOSITION,(UINT_PTR)childMenu.m_hMenu,menuName);
	AfxGetMainWnd()->GetActiveWindow()->DrawMenuBar();
	//---------------
}

//-------------------------------------
//
//	 在此添加命令处理程序代码
//-------------------------------------	
BOOL CPlatformView::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
	// If pHandlerInfo is NULL, then handle the message
	if (pHandlerInfo == NULL)
	{
		if (nCode == CN_COMMAND)
		{
			// Handle WM_COMMAND message
			if (nID==ID_MENU_OPTION)
			{
				if (m_AddMenu1) return TRUE;
				BeginWaitCursor(); 
				CheckInPlugin(CATID_LMHPluginCategory);
				RefreshMenu(m_child1Menu,"插件");
				m_AddMenu1=true;
				EndWaitCursor(); 
				return TRUE;
			}

			if (nID==ID_ACTIVEXMENU)
			{	
				if (m_AddMenu2) return TRUE;
				BeginWaitCursor(); 
				CheckInActiveX(CATID_LMHActiveXCategory);
				RefreshMenu(m_child2Menu,"控件");
				m_AddMenu2=true;
				EndWaitCursor(); 
				return TRUE;
				
			}
			if ((nID>=m_basicMenuID) && (nID<m_currentMenuID))
			{
				CLSID clsID;
				short actionID;
				clsID=m_MenuPluginList.Find(nID,actionID);
				//--------------------------------------------
				if (actionID==-1)		//调用控件功能
					CallActiveX(clsID);
				else					//调用插件功能
					CallPlugin(clsID,actionID);
				//--------------------------------------------
				return TRUE;
					
			}	
										
		}
		else if (nCode == CN_UPDATE_COMMAND_UI)
		{
			// 激活加载菜单项
			if (nID==ID_MENU_OPTION)
			{
				((CCmdUI*)pExtra)->Enable(TRUE);
				return TRUE;
			}

			if (nID==ID_ACTIVEXMENU)
		
			{
				((CCmdUI*)pExtra)->Enable(TRUE);
				return TRUE;
			}
			//---激活PLUGIN菜单项------------------------
			if ((nID>=m_basicMenuID) && (nID<m_currentMenuID))
			{
				((CCmdUI*)pExtra)->Enable(TRUE);
				return TRUE;
			}
			//----------------------------------------------					

		}
		
	}
	return CScrollView::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}



//////////////////////////////////////////////////
//函数：
//功能：Convert a CLSID to a char string.
// 
///////////////////////////////////////////////////
void CLSIDtochar(   const CLSID& clsid,
					char* szCLSID,
					int length)
{
	assert(length >= CLSID_STRING_SIZE) ;
	// Get CLSID
	LPOLESTR wszCLSID = NULL ;
	HRESULT hr = StringFromCLSID(clsid, &wszCLSID) ;
	assert(SUCCEEDED(hr)) ;

	// Convert from wide characters to non-wide characters.
	wcstombs(szCLSID, wszCLSID, length);

	// Free memory.
	CoTaskMemFree(wszCLSID) ;
}



///////////////////////////////////////////////////////////
//函数：
//功能：获得COM类的名
//时间：
//------------------------
BOOL getFriendlyName(const CLSID& clsid, LPCTSTR szFriendlyName, int iLength)
{
	HKEY hKey;
	char szKeyBuf[1024] ;
	char szCLSID[CLSID_STRING_SIZE] ;

	// Convert the clsid to a string.
	CLSIDtochar(clsid, szCLSID, CLSID_STRING_SIZE) ;

	// Make the key.
	sprintf(szKeyBuf, "CLSID\\%s", szCLSID) ;

	// Create and open key and subkey.
	long lResult = RegOpenKeyEx(    HKEY_CLASSES_ROOT ,
									szKeyBuf,
									0,
									KEY_ALL_ACCESS,
									&hKey) ;
	if (lResult != ERROR_SUCCESS)
	{
		return FALSE ;
	}

	// Set the Value.
	ASSERT(szFriendlyName != NULL) ;
	DWORD dwSize = iLength ;
	lResult = RegQueryValueEx( hKey, NULL, NULL, NULL, (BYTE *)szFriendlyName, &dwSize);

	RegCloseKey(hKey);

	return lResult == ERROR_SUCCESS;
}




//---------------------------------------
//函数：CheckInPlugin(GUID catID)
//功能：用于注册Compotent Category
//输入：Compotent Category的GUID
//输出：成功为TRUE，失败为FALSE
//时间：2003/6/17 PM3:10
//作者：
//---------------------------------------
BOOL CPlatformView::CheckInPlugin(GUID catID)
{
	// Create the standard COM Category Manager
	ICatInformation* pICatInformation = NULL ;
	HRESULT hr = ::CoCreateInstance(    CLSID_StdComponentCategoriesMgr,
										NULL, CLSCTX_ALL, IID_ICatInformation,
										(void**)&pICatInformation) ;
	if (FAILED(hr))
	{
		ASSERT(hr) ;
		return FALSE;
	}

	// Array of Categories
	int cIDs = 1 ;
	CATID IDs[1] ;
	IDs[0] = catID; //组件目录的GUID

	// Get the IEnumCLSID interface.
	IEnumCLSID* pIEnumCLSID = NULL ;
	hr = pICatInformation->EnumClassesOfCategories(cIDs, IDs, 0, NULL, &pIEnumCLSID) ;
	ASSERT(SUCCEEDED(hr)) ;

	// Get the next CLSID in the list.
	//char szFriendlyName[128] ;

	CLSID clsid ;
	IPlugin * pPlugin;
	BOOL firstPlugin=true;
	while ((hr = pIEnumCLSID->Next(1, &clsid, NULL)) == S_OK)
	{	
		//---------创建查询到的COM------------------------
		hr = ::CoCreateInstance(clsid,				//COM类的ID
								NULL,
								CLSCTX_INPROC_SERVER,
								IID_IPlugin,		//统一接口定义
								reinterpret_cast<void**>(&pPlugin)) ; //接口指针
		if (FAILED(hr))
		{
			AfxMessageBox("Failed to create the component.") ;
			return FALSE ;
		}
		int i;
		short nCount;
		char funName[80];
		pPlugin->GetFunctionCount(&nCount);
			//-------------------------
		HBITMAP hBitmap;
		pPlugin->GetToolBarBitmap(&hBitmap);
		char pluginName[80];
		pPlugin->GetPluginName(pluginName);
		((CMainFrame*)AfxGetMainWnd())->AddPluginToolbar(hBitmap,m_currentMenuID,nCount,pluginName);
			//--------------------------
		//规定动作序号从0开始
		for (i=0;i<nCount;i++) 
		{
			m_MenuPluginList.Add(clsid,i);
			pPlugin->GetFunctionName(i,funName);
			if ((!firstPlugin) && (i==0))
				RefreshChildMenu(m_child1Menu,true,funName);

			else
				RefreshChildMenu(m_child1Menu,false,funName);

		}
		pPlugin->Release();
		firstPlugin=false;
	}
	pICatInformation->Release() ;

	m_pServer= new CComObject<CServer>;
	_ASSERT(m_pServer != NULL);
	CPlatformDoc* pDoc = GetDocument();
	m_pServer->m_pDoc=pDoc;
	m_pServer->m_pView=this;
	((IServer*)m_pServer)->AddRef();

	return TRUE;
}


//-------------------------------------
//函数：RefreshChildMenu(BOOL seprator,char* menuName)
//功能：用于生成菜单的子项
//输入：seprator为是否为分隔符, menuName为菜单的名字
//输出：无
//时间：2003/6/18 PM4:00
//作者：
//-------------------------------------
void CPlatformView::RefreshChildMenu(CMenu &childMenu,BOOL seprator,char* menuName)
{
	//为主菜单生动完全动态的子菜单项，每项与一COM方法相关联
	//时间：2003/6/18 PM3:00
	//if (m_AddMenu) return;
	if (seprator)
	{
		childMenu.AppendMenu(MF_SEPARATOR);
	}
	childMenu.AppendMenu(MF_STRING,m_currentMenuID, menuName);
	m_currentMenuID++;
}

//---------------------------------------
//函数：CallPlugin(CLSID classID, short actionID)
//功能：用于调用插件的功能
//输入：classID为COM的类ID ,actionID为功能的序号
//输出：成功为TRUE，失败为FALSE
//时间：2003/6/18 PM4:00
//作者：
//---------------------------------------
BOOL CPlatformView::CallPlugin(CLSID classID, short actionID)
{
	HRESULT hr ;
	IPlugin * pPlugin;
	pPlugin=(IPlugin*)m_interfaceList.Find(classID);

	if (pPlugin==NULL)
	{
		//创建查询到的COM
		hr = ::CoCreateInstance(classID,			//COM类的ID
								NULL,
								CLSCTX_INPROC_SERVER,
								IID_IPlugin,//统一接口定义
								reinterpret_cast<void**>(&pPlugin)) ; //接口指针
		if (FAILED(hr))
		{
			AfxMessageBox("Failed to create the component.") ;
			return FALSE ;
		}
		m_interfaceList.Add(classID,pPlugin);

	}
	CDC* cDC=GetDC( );
	pPlugin->PassHDC(cDC->m_hDC);
	CWnd* cWND=AfxGetMainWnd();
	pPlugin->PassHWND(cWND->m_hWnd);
	pPlugin->SetServer(((IUnknown*)m_pServer));
	pPlugin->DoFunction(actionID);

	//pPlugin->Release();
	return TRUE;

}

void CPlatformView::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	SendMouseMessage(WM_MOUSEMOVE,nFlags,point.x,point.y);
	CScrollView::OnMouseMove(nFlags, point);
}


//---------------------------------------------------------
//此函数用于消息传递，向各需要的插件传递鼠标消息
//---------------------------------------------------------
void CPlatformView::SendMouseMessage(UINT Message, UINT flags, int x, int y)
{
	POSITION pos=m_interfaceList.m_interfaceList.GetHeadPosition();
	CInterfaceItem *mItem;
	while(pos!=NULL)
	{
		mItem=m_interfaceList.m_interfaceList.GetAt(pos);
		IPlugin * pPlugin;
		pPlugin=(IPlugin*)(	mItem->m_interface);
		pPlugin->SendMouseMessage(Message,flags,x,y);
		m_interfaceList.m_interfaceList.GetNext(pos);

	}
}

void CPlatformView::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	SendMouseMessage(WM_LBUTTONDBLCLK,nFlags,point.x,point.y);
	CScrollView::OnLButtonDblClk(nFlags, point);
}

void CPlatformView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	SendMouseMessage(WM_LBUTTONDOWN,nFlags,point.x,point.y);
	CScrollView::OnLButtonDown(nFlags, point);
}

void CPlatformView::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	SendMouseMessage(WM_LBUTTONUP,nFlags,point.x,point.y);
	CScrollView::OnLButtonUp(nFlags, point);
}

//---------------------------------------
//函数：CheckInActiveX(GUID catID)
//功能：用于注册Compotent Category,专用于ActiveX组件
//输入：Compotent Category的GUID
//输出：成功为TRUE，失败为FALSE
//时间：2003/6/30 AM11:20
//作者：
//---------------------------------------
BOOL CPlatformView::CheckInActiveX(GUID catID)
{
	// Create the standard COM Category Manager
	ICatInformation* pICatInformation = NULL ;
	HRESULT hr = ::CoCreateInstance(    CLSID_StdComponentCategoriesMgr,
										NULL, CLSCTX_ALL, IID_ICatInformation,
										(void**)&pICatInformation) ;
	if (FAILED(hr))
	{
		ASSERT(hr) ;
		return FALSE;
	}

	// Array of Categories
	int cIDs = 1 ;
	CATID IDs[1] ;
	IDs[0] = catID; //组件目录的GUID

	// Get the IEnumCLSID interface.
	IEnumCLSID* pIEnumCLSID = NULL ;
	hr = pICatInformation->EnumClassesOfCategories(cIDs, IDs, 0, NULL, &pIEnumCLSID) ;
	ASSERT(SUCCEEDED(hr)) ;

	char szFriendlyName[128] ;

	CLSID clsid ;
	while ((hr = pIEnumCLSID->Next(1, &clsid, NULL)) == S_OK)
	{	
		m_MenuPluginList.Add(clsid,-1);
		getFriendlyName(clsid, szFriendlyName, sizeof(szFriendlyName));
		RefreshChildMenu(m_child2Menu,false,szFriendlyName);
	}
	pICatInformation->Release() ;

	return TRUE;
}


//---------------------------------------
//函数：CallActiveX(CLSID classID)
//功能：用于调用控件的功能
//输入：classID为ActiveX的类ID
//输出：成功为TRUE，失败为FALSE
//时间：2003/6/30 PM3:10
//作者：
//---------------------------------------
BOOL CPlatformView::CallActiveX(CLSID classID)
{
	//HRESULT hr ;
	COcxCallDlg * pOcxDlg;
	pOcxDlg=m_ActiveXList.Find(classID);

	if (pOcxDlg==NULL)
	{
		//创建查询到的ActiveX调用对话框
		pOcxDlg=new COcxCallDlg(classID);
		//pOcxDlg->m_classID=classID;
		pOcxDlg->Create(IDD_OCXCALLDLG);
		
        m_ActiveXList.Add(classID,pOcxDlg);

	}

	pOcxDlg->ShowWindow(SW_SHOWNORMAL);

	//pPlugin->Release();
	return TRUE;
}
