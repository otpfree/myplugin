//----------------------------------
//
//公用COLLECTION结构定义
//
//----------------------------------
#pragma once
#include <afxtempl.h>
#include "ocxcalldlg.h"
class COcxCallDlg;
//-------------------------------
class CMenuItem
{
public:
	UINT m_menuID;
	CLSID m_classID;
	short m_menuAction;

public:
	CMenuItem(void);

	CMenuItem(UINT menuID,CLSID classID,short menuAction);
	
	~CMenuItem(void);

};



typedef CTypedPtrList <CPtrList,CMenuItem*> CMenuList;
//---------------------------------------------

class CMenuPlugin
{
public:
	CMenuPlugin(void);
	~CMenuPlugin(void);
	CMenuList m_menuList;
private:
	UINT m_firstMenuID,m_CurrentMenuID;
	int m_menuCount;
public:
	void InitMenu(UINT menuID);
	BOOL Add(CLSID classID, short menuAction);
	CLSID Find(UINT menuID,short& actionID);
	void ClearAll(void);
};


//-------------------------------------------------

//-------------------------------------------------
class CInterfaceItem
{
public:
	CLSID m_classID;
	IUnknown* m_interface;

public:
	CInterfaceItem(void);
	~CInterfaceItem(void);
	
};

typedef CTypedPtrList <CPtrList,CInterfaceItem*> CInterfaceList;

//--------------------------------------------------
class CInterfaceDB
{
public:
	CInterfaceDB(void);
	~CInterfaceDB(void);
	CInterfaceList	m_interfaceList;

	BOOL Add(CLSID classID, IUnknown* interfaceName);
	IUnknown* Find(CLSID classID);
	void ClearAll(void);
};


class CActiveXItem
{
public:
	CLSID m_classID;
	COcxCallDlg* m_ocxDlg;

public:
	CActiveXItem(void);
	~CActiveXItem(void);
	
};

typedef CTypedPtrList <CPtrList,CActiveXItem*> CActiveXList;

class CActiveXDB
{
public:
	CActiveXDB(void);
	~CActiveXDB(void);
	CActiveXList	m_activexList;
	BOOL Add(CLSID classID, COcxCallDlg* pocxCallDlg);
	void ClearAll(void);
	COcxCallDlg* Find(CLSID classID);
};    
//注：有时一个小分号";"，就会忙的你直真抓瞎，找不着北！  刘明辉  2003/6/30 AM11:04

