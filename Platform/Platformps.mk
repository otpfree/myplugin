
Platformps.dll: dlldata.obj Platform_p.obj Platform_i.obj
	link /dll /out:Platformps.dll /def:Platformps.def /entry:DllMain dlldata.obj Platform_p.obj Platform_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \
.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		$<

clean:
	@del Platformps.dll
	@del Platformps.lib
	@del Platformps.exp
	@del dlldata.obj
	@del Platform_p.obj
	@del Platform_i.obj
