// MainFrm.h : CMainFrame 类的接口
//
#pragma once
#include "toolbaritem.h"

class CMainFrame : public CFrameWnd
{
	
protected: // 仅从序列化创建
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// 属性
public:

// 操作
public:

// 重写
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// 实现
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // 控件条嵌入成员
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
	//CToolBar    m_wndToolBarAdd1;
	CToolBarDB	m_pluginToolBarList;
	UINT        m_baseToolID,m_currentToolID;
	//CCSPaper    m_wndOutput;*/

// 生成的消息映射函数
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_MESSAGE_MAP()
public:
//	afx_msg void OnAddtoolbar();
	afx_msg BOOL OnToolTipNotify( UINT id, NMHDR * pNMHDR, LRESULT * pResult );
	void AddPluginToolbar(HBITMAP hBitmap,UINT iMenuID,int iCount,char* cName);
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	void DockControlBarLeftOf(CToolBar* Bar, CToolBar* LeftOf);

//	afx_msg void OnNcMouseMove(UINT nHitTest, CPoint point);
//	afx_msg UINT OnNcHitTest(CPoint point);
};


