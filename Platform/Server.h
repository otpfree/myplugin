// Server.h : CServer ������

#pragma once
#include "resource.h"       // ������

#include "Platform.h"

#include "platformdoc.h"
#include "platformview.h"
// CServer
class CPlatformDoc;
class CPlatformView;
class ATL_NO_VTABLE CServer : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CServer, &CLSID_Server>,
	public IServer
{
public:
	CServer()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_SERVER)


BEGIN_COM_MAP(CServer)
	COM_INTERFACE_ENTRY(IServer)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}
	
	STDMETHOD(GetDataPoint)(myGraph** dPointer);

public:
	CPlatformDoc* m_pDoc;
	CPlatformView* m_pView;

	STDMETHOD(NewDataCircle)(myGraph* pGraph);
};

OBJECT_ENTRY_AUTO(__uuidof(Server), CServer)
