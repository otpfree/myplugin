#pragma once
#define IDD_OCXCALLDLG 106

// COcxCallDlg 对话框

class COcxCallDlg : public CDialog
{
	DECLARE_DYNAMIC(COcxCallDlg)

public:
	COcxCallDlg(CWnd* pParent = NULL);   // 标准构造函数
	COcxCallDlg(CLSID classID,CWnd* pParent = NULL); 

	virtual ~COcxCallDlg();
	// 对话框数据
	enum { IDD = IDD_OCXCALLDLG };

	CWnd     m_ocxWnd;		//调用OCX控件窗口
	CLSID    m_classID;		//被调用的ocx控件的CLSID
	UINT	 m_curActivXID;
	

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
};
