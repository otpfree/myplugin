// Server.cpp : CServer 的实现

#include "stdafx.h"
#include "Server.h"


// CServer
STDMETHODIMP CServer::GetDataPoint(myGraph** dPointer)
{
	AFX_MANAGE_STATE(AfxGetAppModuleState());

	// TODO: 在此添加实现代码
	//myGraph *mGraph=new myGraph;
	//mGraph=m_pDoc->m_GraphHead;
	*dPointer=m_pDoc->m_GraphHead;

	return S_OK;
}

STDMETHODIMP CServer::NewDataCircle(myGraph* pGraph)
{
	AFX_MANAGE_STATE(AfxGetAppModuleState());
	myGraph* mGraph;
	if (m_pDoc->m_GraphHead==NULL)
	{
		m_pDoc->m_GraphHead=new myGraph;
		m_pDoc->m_GraphHead->mWidth =pGraph->mWidth;
		m_pDoc->m_GraphHead->mRed=pGraph->mRed;
		m_pDoc->m_GraphHead->mGreen=pGraph->mGreen;
		m_pDoc->m_GraphHead->mBlue=pGraph->mBlue;
		m_pDoc->m_GraphHead->x=pGraph->x;
		m_pDoc->m_GraphHead->y=pGraph->y;
		m_pDoc->m_GraphHead->mRate=pGraph->mRate;
		m_pDoc->m_GraphHead->next=NULL;
		m_pDoc->m_GraphTail=m_pDoc->m_GraphHead;
		//m_pDoc->SetModifiedFlag();

	}
	else
	{	mGraph=new myGraph;
		m_pDoc->m_GraphTail->next=mGraph;
		m_pDoc->m_GraphTail=mGraph;
		m_pDoc->m_GraphTail->mWidth=pGraph->mWidth;
		m_pDoc->m_GraphTail->mRed=pGraph->mRed;
		m_pDoc->m_GraphTail->mGreen=pGraph->mGreen;
		m_pDoc->m_GraphTail->mBlue=pGraph->mBlue;
		m_pDoc->m_GraphTail->x=pGraph->x;
		m_pDoc->m_GraphTail->y=pGraph->y;
		m_pDoc->m_GraphTail->mRate=pGraph->mRate;
		m_pDoc->m_GraphTail->next=NULL;

	}
	m_pDoc->m_countGraph++;
	m_pDoc->SetModifiedFlag();

	return S_OK;
}
