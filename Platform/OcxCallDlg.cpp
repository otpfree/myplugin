// OcxCallDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "Platform.h"
#include "OcxCallDlg.h"


// COcxCallDlg 对话框

IMPLEMENT_DYNAMIC(COcxCallDlg, CDialog)
COcxCallDlg::COcxCallDlg(CWnd* pParent /*=NULL*/)
	: CDialog(COcxCallDlg::IDD, pParent)
{
}

COcxCallDlg::COcxCallDlg(CLSID classID, CWnd* pParent)
	: CDialog(COcxCallDlg::IDD, pParent)
{
	m_classID=classID;
	m_curActivXID=ID_VIEWCONTROL;

}
COcxCallDlg::~COcxCallDlg()
{
	if (m_ocxWnd.m_hWnd!=NULL)
		m_ocxWnd.DestroyWindow();
}

void COcxCallDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(COcxCallDlg, CDialog)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// COcxCallDlg 消息处理程序

BOOL COcxCallDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	//------用于测试对话框的控件调用方式------------
	//static CLSID const clsid= { 0x479B29EF, 0x9A2C, 0x11D0, { 0xB6, 0x96, 0x0, 0xA0, 0xC9, 0x3, 0x48, 0x7A } };
	//m_classID= clsid;
	//-----------------------------------------------
	RECT rt;
	::GetClientRect(this->m_hWnd,&rt);
	m_ocxWnd.CreateControl(m_classID,"test",WS_VISIBLE,rt,this,m_curActivXID++);
	m_ocxWnd.ShowWindow(SW_SHOW);
	//-----------------------

	return TRUE;
}

void COcxCallDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);
	
	//-----------放大插入的控件-------------
	if (m_ocxWnd.m_hWnd!=NULL)
		m_ocxWnd.MoveWindow(0,0,cx,cy);
		//m_ocxWnd.OnSize(nType,cx,cy);
	
}
