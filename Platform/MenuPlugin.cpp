#include "StdAfx.h"
#include "menuplugin.h"


CMenuItem::CMenuItem()
{
	m_menuID=0;

}
CMenuItem::CMenuItem(UINT menuID,CLSID classID,short menuAction)
{
	m_menuID=menuID;
	m_classID=classID;
	m_menuAction=menuAction;

}
CMenuItem::~CMenuItem()
{
	
}


CMenuPlugin::CMenuPlugin(void)
{
}

CMenuPlugin::~CMenuPlugin(void)
{
}

void CMenuPlugin::InitMenu(UINT menuID)
{
	m_firstMenuID=menuID;
	m_CurrentMenuID=menuID;
	m_menuCount=0;

}

BOOL CMenuPlugin::Add(CLSID classID, short menuAction)
{
	CMenuItem *mItem;
	m_menuCount++;
	mItem=new CMenuItem(m_CurrentMenuID,classID,menuAction);
	m_CurrentMenuID++;
	m_menuList.AddTail(mItem);
	return true;
}

CLSID CMenuPlugin::Find(UINT menuID, short& actionID)
{
	POSITION pos=m_menuList.GetHeadPosition();
	CMenuItem *mItem;
	while(pos!=NULL)
	{
		mItem=m_menuList.GetAt(pos);
		if (mItem->m_menuID==menuID)
		{
			actionID=mItem->m_menuAction;
			return mItem->m_classID;

		}

		m_menuList.GetNext(pos);

	}
	actionID=-1;  //表示未找到！
	return CLSID();
}

void CMenuPlugin::ClearAll(void)
{
	POSITION pos=m_menuList.GetHeadPosition();
	//CMenuItem *mItem;
	while(pos!=NULL)
	{
		delete	m_menuList.GetNext(pos);

	}
	m_menuList.RemoveAll();

}



CInterfaceItem::CInterfaceItem()
{
	m_interface=NULL;
}

CInterfaceItem::~CInterfaceItem()
{
}

//
//-----------------------------------------------
CInterfaceDB::CInterfaceDB(void)
{
}

CInterfaceDB::~CInterfaceDB(void)
{
}

BOOL CInterfaceDB::Add(CLSID classID, IUnknown* interfaceName)
{
	//------------------------------------
	CInterfaceItem *mItem;
	mItem=new CInterfaceItem();
	mItem->m_classID=classID;
	mItem->m_interface=interfaceName;
	m_interfaceList.AddTail(mItem);
	return true;
	//------------------------------------
}

IUnknown* CInterfaceDB::Find(CLSID classID)
{
	POSITION pos=m_interfaceList.GetHeadPosition();
	CInterfaceItem *mItem;
	while(pos!=NULL)
	{
		mItem=m_interfaceList.GetAt(pos);
		if (mItem->m_classID==classID)
		{
			return mItem->m_interface;
		}

		m_interfaceList.GetNext(pos);

	}
	return NULL;//表示未找到！
}

void CInterfaceDB::ClearAll(void)
{
	POSITION pos=m_interfaceList.GetHeadPosition();
	while(pos!=NULL)
	{
		delete	m_interfaceList.GetNext(pos);
	}
	m_interfaceList.RemoveAll();
}

//---------------------------------
CActiveXItem::CActiveXItem()
{

}

CActiveXItem::~CActiveXItem()
{

}

CActiveXDB::CActiveXDB()
{
}

CActiveXDB::~CActiveXDB()
{

}

BOOL CActiveXDB::Add(CLSID classID, COcxCallDlg* pocxCallDlg)
{
	//------------------------------------
	CActiveXItem *mItem;
	mItem=new CActiveXItem();
	mItem->m_classID=classID;
	mItem->m_ocxDlg=pocxCallDlg;
	m_activexList.AddTail(mItem);
	return true;
	//------------
}

void CActiveXDB::ClearAll(void)
{
	POSITION pos=m_activexList.GetHeadPosition();
	while(pos!=NULL)
	{
		delete	m_activexList.GetNext(pos);
	}
	m_activexList.RemoveAll();
}

COcxCallDlg* CActiveXDB::Find(CLSID classID)
{
	POSITION pos=m_activexList.GetHeadPosition();
	CActiveXItem *mItem;
	while(pos!=NULL)
	{
		mItem=m_activexList.GetAt(pos);
		if (mItem->m_classID==classID)
		{
			return mItem->m_ocxDlg;
		}

		m_activexList.GetNext(pos);

	}
	return NULL;//表示未找到！
}
