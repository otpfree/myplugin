// PlatformDoc.cpp :  CPlatformDoc 类的实现
//

#include "stdafx.h"
#include "Platform.h"

#include "PlatformDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CPlatformDoc

IMPLEMENT_DYNCREATE(CPlatformDoc, CDocument)

BEGIN_MESSAGE_MAP(CPlatformDoc, CDocument)
//	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
//ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
END_MESSAGE_MAP()


// CPlatformDoc 构造/销毁

CPlatformDoc::CPlatformDoc()
{
	m_GraphHead=NULL;
	m_GraphTail=NULL;
	m_countGraph=0;
	//m_GraphHead=new myGraph;
	//m_GraphHead->mColor=RGB(255,0,0);
	//POINT pt;pt.x=100;pt.y=100;
	//m_GraphHead->mPt=pt;	//POINT(100,100);
	//m_GraphHead->mRate=2;
	//m_GraphHead->mWidth=3;
	//m_GraphHead->next=NULL;

}

CPlatformDoc::~CPlatformDoc()
{
	//
}

BOOL CPlatformDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO：在此添加重新初始化代码
	// （SDI 文档将重用该文档）
	m_GraphHead=NULL;
	m_GraphTail=NULL;
	m_countGraph=0;
	return TRUE;
	
}




// CPlatformDoc 序列化

void CPlatformDoc::Serialize(CArchive& ar)
{	//----------------------------------	
	myGraph *mGraph;
	mGraph=m_GraphHead;
	if (ar.IsStoring())
	{
		// TODO：在此添加存储代码
		ar<<m_countGraph;
		while (mGraph!=NULL)
		{
			ar<<mGraph->mWidth;
			ar<<mGraph->mRed;
			ar<<mGraph->mGreen;
			ar<<mGraph->mBlue;
			ar<<mGraph->x;
			ar<<mGraph->y;
			ar<<mGraph->mRate;
			mGraph=mGraph->next;
			
		}


	}
	else
	{
		// TODO：在此添加加载代码
		ar>>m_countGraph;
		m_GraphHead=NULL;//new myGraph;
		m_GraphTail=NULL;//m_GraphHead;
		for(int i=1;i<=m_countGraph;i++)
		{
			if (m_GraphHead==NULL)
			{
				m_GraphHead=new myGraph;
				ar>>m_GraphHead->mWidth;
				ar>>m_GraphHead->mRed;
				ar>>m_GraphHead->mGreen;
				ar>>m_GraphHead->mBlue;
				ar>>m_GraphHead->x;
				ar>>m_GraphHead->y;
				ar>>m_GraphHead->mRate;
				m_GraphHead->next=NULL;
				m_GraphTail=m_GraphHead;

			}
			else
			{	mGraph=new myGraph;
				m_GraphTail->next=mGraph;
				m_GraphTail=mGraph;
				ar>>m_GraphTail->mWidth;
				ar>>m_GraphTail->mRed;
				ar>>m_GraphTail->mGreen;
				ar>>m_GraphTail->mBlue;
				ar>>m_GraphTail->x;
				ar>>m_GraphTail->y;
				ar>>m_GraphTail->mRate;
				m_GraphTail->next=NULL;

			}

		}
	}
}


// CPlatformDoc 诊断

#ifdef _DEBUG
void CPlatformDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CPlatformDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CPlatformDoc 命令

void CPlatformDoc::DeleteContents()
{
	// TODO: 在此添加专用代码和/或调用基类
	myGraph* mGraph;
	
	while(m_GraphHead!=NULL)
	{	
		mGraph=m_GraphHead;
		m_GraphHead=m_GraphHead->next;
		delete mGraph;
		
	}
	m_GraphHead=NULL;
	m_GraphTail=NULL;

	CDocument::DeleteContents();
}

//BOOL CPlatformDoc::OnOpenDocument(LPCTSTR lpszPathName)
//{
//	if (!CDocument::OnOpenDocument(lpszPathName))
//		return FALSE;
//
//	// TODO:  在此添加您专用的创建代码
//
//	return TRUE;
//}

//void CPlatformDoc::OnFileOpen()
//{
//// TODO: 在此添加命令处理程序代码
//	static char BASED_CODE szFilter[] = "Circle Files (*.cir)|*.cir|Graph Files (*.gra)|*.gra|All Files (*.*)|*.*||";
//    CFileDialog fileDlg (TRUE, "Circle files", "*.cir",OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, szFilter, NULL);
//	if( fileDlg.DoModal ()==IDOK )
//	{
//		CString pathName = fileDlg.GetPathName();
//
//		OnOpenDocument(LPCTSTR(pathName));
//		UpdateAllViews(NULL);
//	
//	}
//}

//void CPlatformDoc::OnFileOpen()
//{
//	// TODO: 在此添加命令处理程序代码
//	
//}
