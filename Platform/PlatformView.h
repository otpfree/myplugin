// PlatformView.h : CPlatformView 类的接口
//

#pragma once
#include "server.h"
#include "menuplugin.h"
#include "OcxCallDlg.h"


class CMenuPlugin;
class CServer;
class CPlatformView : public CScrollView
{
protected: // 仅从序列化创建
	CPlatformView();
	DECLARE_DYNCREATE(CPlatformView)

// 属性
public:
	CPlatformDoc* GetDocument() const;
	CMenu m_child1Menu , m_child2Menu;//用于动态的主菜单项
	BOOL m_AddMenu1,m_AddMenu2;  //动态菜单是否已创建，初始值为FALSE+

	class CServer* m_pServer; 
	COcxCallDlg* m_pDlg;

// 操作
public:

// 重写
	public:
	virtual void OnDraw(CDC* pDC);  // 重写以绘制该视图
virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void OnInitialUpdate(); // 构造后第一次调用
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 实现
public:
	virtual ~CPlatformView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
protected:
	void DoPopupMenu(UINT nMenuID);
public:
	//afx_msg void OnPopupCommand();
	afx_msg void OnPopupCommand(UINT nMenuID);
	
	void RefreshMenu(CMenu &childMenu,char* menuName);
	void RefreshChildMenu(CMenu &childMenu,BOOL seprator,char* menuName);
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	BOOL CheckInPlugin(GUID catID);
	UINT m_basicMenuID;
	UINT m_currentMenuID;

protected:
	CMenuPlugin m_MenuPluginList;
	CInterfaceDB m_interfaceList;
	CActiveXDB   m_ActiveXList;
public:
	BOOL CallPlugin(CLSID classID, short actionID);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	void SendMouseMessage(UINT Message, UINT flags, int x, int y);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	BOOL CheckInActiveX(GUID catID);
	BOOL CallActiveX(CLSID classID);

};

#ifndef _DEBUG  // PlatformView.cpp 的调试版本
inline CPlatformDoc* CPlatformView::GetDocument() const
   { return reinterpret_cast<CPlatformDoc*>(m_pDocument); }
#endif

