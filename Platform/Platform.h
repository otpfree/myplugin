// Platform.h : Platform 应用程序的主头文件
//
#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // 主符号
#include "Platform_i.h"


// CPlatformApp:
// 有关此类的实现，请参阅 Platform.cpp
//

class CPlatformApp : public CWinApp
{
public:
	CPlatformApp();


// 重写
public:
	virtual BOOL InitInstance();

// 实现
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
	BOOL ExitInstance(void);
//	afx_msg void OnFileOpen();
//	virtual CDocument* OpenDocumentFile(LPCTSTR lpszFileName);
};

extern CPlatformApp theApp;
