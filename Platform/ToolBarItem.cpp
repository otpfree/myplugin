#include "StdAfx.h"
#include "toolbaritem.h"

CToolBarItem::CToolBarItem(void)
{
}

CToolBarItem::~CToolBarItem(void)
{
}

CToolBarDB::CToolBarDB(void)
{

}

CToolBarDB::~CToolBarDB(void)
{

}
BOOL CToolBarDB::Add(UINT menuID, CToolBar* mToolbar)
{
	CToolBarItem *mItem;
	mItem=new CToolBarItem();
	mItem->m_iMenuID=menuID;
	mItem->m_pToolBar=mToolbar;
	m_toolbarList.AddTail(mItem);
	return true;
}

void CToolBarDB::ClearAll(void)
{
	POSITION pos=m_toolbarList.GetHeadPosition();
	while(pos!=NULL)
	{
		delete	m_toolbarList.GetNext(pos);
	}
	m_toolbarList.RemoveAll();
}

CToolBar* CToolBarDB::Find(UINT menuID)
{
	POSITION pos=m_toolbarList.GetHeadPosition();
	CToolBarItem *mItem;
	while(pos!=NULL)
	{
		mItem=m_toolbarList.GetAt(pos);
		if (mItem->m_iMenuID==menuID)
		{
			return mItem->m_pToolBar;
		}
		m_toolbarList.GetNext(pos);
	}
	return NULL;//��ʾδ�ҵ���
}

CToolBar* CToolBarDB::GetLast(void)
{
	POSITION pos=m_toolbarList.GetTailPosition();
	CToolBarItem *mItem;
	if(pos!=NULL)
	{
		mItem=m_toolbarList.GetAt(pos);
		return mItem->m_pToolBar;
	}
	else
        return NULL;
}
