#pragma once
//#include "toolbaritem.h"
#include <afxtempl.h>
class CToolBarItem
{
public:
	UINT m_iMenuID;
	CToolBar* m_pToolBar;
public:
	CToolBarItem(void);
	~CToolBarItem(void);
};


typedef  CTypedPtrList<CPtrList,CToolBarItem*>   CToolBarList;

class CToolBarDB
{
public:
	CToolBarDB(void);
	~CToolBarDB(void);
	CToolBarList	m_toolbarList;
	
	BOOL Add(UINT menuID, CToolBar* mToolbar);
	void ClearAll(void);
	CToolBar* Find(UINT menuID);
	CToolBar* GetLast(void);
};    