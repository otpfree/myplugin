// PlatformDoc.h :  CPlatformDoc 类的接口
//


#pragma once

//#include "commstruct.h"

class CPlatformDoc : public CDocument
{
protected: // 仅从序列化创建
	CPlatformDoc();
	DECLARE_DYNCREATE(CPlatformDoc)

// 属性
public:
	//----------------------------------
	myGraph *m_GraphHead, *m_GraphTail;
	int m_countGraph;
// 操作
public:

// 重写
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	

// 实现
public:
	virtual ~CPlatformDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual void DeleteContents();
//	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
//	afx_msg void OnFileOpen();
//	afx_msg void OnFileOpen();
};


