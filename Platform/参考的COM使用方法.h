#pragma once

//------------------------------------------------------------
//接口定义的一种方式，可以被引用
interface DECLSPEC_UUID("FD1AB5B5-E66C-457D-81B4-6DDFD6B125E3")
IMessageProxy : public IUnknown
{

};

//----------------------------------------------------------------------------
//也是接口定义，可以用于生成连接点
interface DECLSPEC_UUID("9F4ECD95-2580-4E80-A0D1-15DCE194FADB")

IMessage : public IUnknown
{
public:
	virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE MouseMove(UINT flags, int x, int y)=0;
	virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE LMouseDown(UINT flags, int x, int y)=0;
	virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE LMouseUp(UINT flags, int x, int y)=0;
	virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE LMouseDClick(UINT flags, int x, int y)=0;
	virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RMouseDown(UINT flags, int x, int y)=0;
	virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RMouseUp(UINT flags, int x, int y)=0;
	virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RMouseDClick(UINT flags, int x, int y)=0;
};


//----------------------------------------------------------------------------
//IMessage的IDL定义文件的部分
[
	object,
	uuid(9F4ECD95-2580-4E80-A0D1-15DCE194FADB),
	helpstring("IMessage 接口"),
	pointer_default(unique)
]
interface IMessage : IUnknown{
	[helpstring("方法MouseMove")] HRESULT MouseMove(UINT flags, int x, int y);
	[helpstring("方法LMouse")] HRESULT LMouseDown(UINT flags, int x, int y);
	[helpstring("方法LMouseUp")] HRESULT LMouseUp(UINT flags, int x, int y);
	[helpstring("方法LMouseDClick")] HRESULT LMouseDClick(UINT flags, int x, int y);
	[helpstring("方法RMouseDown")] HRESULT RMouseDown(UINT flags, int x, int y);
	[helpstring("方法RMouseUp")] HRESULT RMouseUp(UINT flags, int x, int y);
	[helpstring("方法RMouseDClick")] HRESULT RMouseDClick(UINT flags, int x, int y);
};

//-----------------------------------------------------
//IMessage接口的实现类定义，部分。
class ATL_NO_VTABLE CMessage : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CMessage, &CLSID_Message>,
	public IMessage
{
public:
	CMessage()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_MESSAGE)


BEGIN_COM_MAP(CMessage)
	COM_INTERFACE_ENTRY(IMessage)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

public:

	STDMETHOD(MouseMove)(UINT flags, int x, int y);
	STDMETHOD(LMouseDown)(UINT flags, int x, int y);
	STDMETHOD(LMouseUp)(UINT flags, int x, int y);
	STDMETHOD(LMouseDClick)(UINT flags, int x, int y);
	STDMETHOD(RMouseDown)(UINT flags, int x, int y);
	STDMETHOD(RMouseUp)(UINT flags, int x, int y);
	STDMETHOD(RMouseDClick)(UINT flags, int x, int y);
};

OBJECT_ENTRY_AUTO(__uuidof(Message), CMessage)


//一种接口实现类的使用方法
CMessage* m_pMessage = new CComObject<CMessage>;
_ASSERT(m_pMessage != NULL);
((IMessage*)m_pMessage)->AddRef();
//---------------------------------------------------------------------------

//--------------------------------------------------------------------------
//COM对象的类ID定义，可用于引用生成COM对象
static const CLSID CLSID_MessageProxy = {0x4B29B155,0xAD81,0x4CD4,{0xAF,0xF9,0x48,0xA5,0x72,0xC5,0x25,0x92}};

//接口IMessageProxy的接口ID定义
static const IID    IID_IMessageProxy = {0xFD1AB5B5,0xE66C,0x457D,{0x81,0xB4,0x6D,0xDF,0xD6,0xB1,0x25,0xE3}};

//IMessageProxy接口的IDL定义文件，部分。
import "oaidl.idl";
import "ocidl.idl";

[
	object,
	uuid(FD1AB5B5-E66C-457D-81B4-6DDFD6B125E3),
	helpstring("IMessageProxy 接口"),
	pointer_default(unique)
]
interface IMessageProxy : IUnknown{
};
[
	uuid(6A8BBA0B-DC43-43E3-B4AA-DC2DFA40F5D9),
	version(1.0),
	helpstring("Platform 1.0 类型库")
]
library PlatformLib
{
	importlib("stdole2.tlb");
	[
		uuid(4B29B155-AD81-4CD4-AFF9-48A572C52592),  //具体的COM对象CLSID
		helpstring("MessageProxy Class")
	]
	coclass MessageProxy
	{
		[default] interface IMessageProxy;
	};
};

//--------------------------------------------------------------------------



