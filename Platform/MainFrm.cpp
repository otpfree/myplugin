// MainFrm.cpp : CMainFrame 类的实现
//

#include "stdafx.h"
#include "Platform.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
//	ON_WM_MOUSEMOVE()
	ON_NOTIFY_EX( TTN_NEEDTEXT, 0, OnToolTipNotify )
//	ON_WM_NCMOUSEMOVE()
//	ON_WM_NCHITTEST()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // 状态行指示器
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};


// CMainFrame 构造/销毁

CMainFrame::CMainFrame()
{
	// TODO：在此添加成员初始化代码
	m_baseToolID=m_currentToolID=ID_VIEWTOOLBAR;
}

CMainFrame::~CMainFrame()
{
	m_pluginToolBarList.ClearAll();
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("未能创建工具栏\n");
		return -1;      // 未能创建
	}
	m_wndToolBar.SetWindowText("主工具");

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("未能创建状态栏\n");
		return -1;      // 未能创建
	}
	
	// TODO: 如果不需要工具栏可停靠，则删除这三行
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: 在此处通过修改 CREATESTRUCT cs 来修改窗口类或
	// 样式

	return TRUE;
}


// CMainFrame 诊断

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


// CMainFrame 消息处理程序


//void CMainFrame::OnAddtoolbar()
//{
//	if (m_wndToolBarAdd1.m_hWnd!=NULL) 
//	{	//将关闭的工具条打开
//		BOOL bVisible = ((m_wndToolBarAdd1.IsWindowVisible()) != 0);
//		if (bVisible) return;
//		ShowControlBar(&m_wndToolBarAdd1, !bVisible, FALSE);
//		RecalcLayout();
//		return;
//	}
//
//}


void CMainFrame::AddPluginToolbar(HBITMAP hBitmap,UINT iMenuID,int iCount,char* cName)
{
	CToolBar* pToolBar;
	pToolBar=new CToolBar();

	if (!pToolBar->CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER |  CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC ) )
	{
		TRACE0("未能创建工具栏\n");
     }
	if (!pToolBar->SetBitmap(hBitmap))
	{
		TRACE0("未能创建工具栏位图\n");
	}
	pToolBar->SetButtons(NULL, iCount);
	for (int i=0;i<iCount;i++)
        pToolBar->SetButtonInfo(i, iMenuID+i,TBBS_BUTTON, i);
	
	/*m_wndToolBarAdd1.SetButtonInfo(1, ID_DYNAMENU+1,TBBS_BUTTON, 1);
	m_wndToolBarAdd1.SetButtonInfo(5, ID_DYNAMENU+5,TBBS_BUTTON, 5);
	m_wndToolBarAdd1.SetButtonInfo(7, ID_SEPARATOR,TBBS_SEPARATOR, 6);*/

	//使用工具条能够浮动和有入坞功能
	pToolBar->SetWindowText(cName);
	pToolBar->EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	//DockControlBar(pToolBar);
	CToolBar* pLastTool;
	pLastTool=m_pluginToolBarList.GetLast();
	if (pLastTool==NULL)
		DockControlBarLeftOf(pToolBar,&m_wndToolBar);
	else
		DockControlBarLeftOf(pToolBar,pLastTool);
	this->RecalcLayout();
	// 定位视图子菜单
	CMenu* pViewMenu = NULL;
	CMenu* pTopMenu = GetMenu();
	int iPos;
	for (iPos = pTopMenu->GetMenuItemCount()-1; iPos >= 0; iPos--)
	{
		CMenu* pMenu = pTopMenu->GetSubMenu(iPos);
		if (pMenu && pMenu->GetMenuItemID(0) == ID_VIEW_TOOLBAR)
		{
			pViewMenu = pMenu;
			break;
		}
	}
	//增加插件查看工具条菜单
	if (pLastTool==NULL) pViewMenu->AppendMenu(MF_SEPARATOR);
	pViewMenu->AppendMenu(MF_STRING,m_currentToolID, cName);
	m_pluginToolBarList.Add(m_currentToolID,pToolBar);

	m_currentToolID++;

}

BOOL CMainFrame::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
	// If pHandlerInfo is NULL, then handle the message
	if (pHandlerInfo == NULL)
	{
		if (nCode == CN_COMMAND)
		{
			if ((nID>=m_baseToolID) && (nID<m_currentToolID))
			{
				CToolBar* pToolBar;
				pToolBar=m_pluginToolBarList.Find(nID);
				BOOL bVisible = ((pToolBar->IsWindowVisible()) != 0);
				//if (bVisible) return true;
				ShowControlBar(pToolBar, !bVisible, FALSE);
				RecalcLayout();
				return TRUE;
					
			}	
						
		}
		else if (nCode == CN_UPDATE_COMMAND_UI)
		{
			if ((nID>=m_baseToolID) && (nID<m_currentToolID))
			{
				((CCmdUI*)pExtra)->Enable(TRUE);
				CToolBar* pToolBar;
				pToolBar=m_pluginToolBarList.Find(nID);
				if (pToolBar==NULL) return TRUE;
				BOOL bVisible = ((pToolBar->IsWindowVisible()) != 0);
				((CCmdUI*)pExtra)->SetCheck(bVisible);
				return TRUE;
			}
			//----------------------------------------------					

		}
	
		
	}


	return CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

void CMainFrame::DockControlBarLeftOf(CToolBar* Bar, CToolBar* LeftOf)
{
	CRect rect;
	DWORD dw;
	UINT n;

	// get MFC to adjust the dimensions of all docked ToolBars
	// so that GetWindowRect will be accurate
	RecalcLayout();
	LeftOf->GetWindowRect(&rect);
	rect.OffsetRect(1,0);
	dw=LeftOf->GetBarStyle();
	n = 0;
	n = (dw&CBRS_ALIGN_TOP) ? AFX_IDW_DOCKBAR_TOP : n;
	n = (dw&CBRS_ALIGN_BOTTOM && n==0) ? AFX_IDW_DOCKBAR_BOTTOM : n;
	n = (dw&CBRS_ALIGN_LEFT && n==0) ? AFX_IDW_DOCKBAR_LEFT : n;
	n = (dw&CBRS_ALIGN_RIGHT && n==0) ? AFX_IDW_DOCKBAR_RIGHT : n;

	// When we take the default parameters on rect, DockControlBar will dock
	// each Toolbar on a seperate line.  By calculating a rectangle, we in effect
	// are simulating a Toolbar being dragged to that location and docked.
	DockControlBar(Bar,n,&rect);
}

BOOL  CMainFrame::OnToolTipNotify( UINT id, NMHDR * pNMHDR, LRESULT * pResult )
{
    TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *)pNMHDR;
    UINT nID =UINT(pNMHDR->idFrom);
    {
           
			CMenu* pTopMenu = GetMenu();
			char lpStr[80];
			pTopMenu->GetMenuString(nID,lpStr,80,MF_BYCOMMAND);
			pTTT->lpszText =lpStr;
			strcpy(pTTT->szText,"dddddd");
			//m_wndStatusBar.SetPaneText(0, "My New Status Bar Text", TRUE);
            pTTT->hinst = NULL;
            return(TRUE);
    }
	
}

