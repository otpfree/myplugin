// L1Plugin.h : CL1Plugin 的声明

#pragma once
#include "resource.h"       // 主符号

#include "Plugin.h"

#include "../plugin2/Plugin2.h"
#include "../plugin2/Plugin2_i.c"
#include "iserver.h"
#include "testdlg.h"
// CL1Plugin
const short  FUNCTION_NUM = 3;
extern HINSTANCE global_hInstance;   //全局运行程序实例

class ATL_NO_VTABLE CL1Plugin : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CL1Plugin, &CLSID_L1Plugin>,
	public IL1Plugin,
	public IDispatchImpl<_IPluginEvents, &__uuidof(_IPluginEvents), &LIBID_Plugin2Lib, /* wMajor = */ 1, /* wMinor = */ 0>,
	public IDispatchImpl<IPlugin, &__uuidof(IPlugin), &LIBID_Plugin2Lib, /* wMajor = */ 1, /* wMinor = */ 0>
{
public:
	CL1Plugin()
	{
		m_Dlg=NULL;
	}

	DECLARE_REGISTRY_RESOURCEID(IDR_L1PLUGIN)


	BEGIN_COM_MAP(CL1Plugin)
		COM_INTERFACE_ENTRY(IL1Plugin)
		COM_INTERFACE_ENTRY(_IPluginEvents)
		COM_INTERFACE_ENTRY(IPlugin)
	END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease() 
	{
	}

public:
	IServer* m_pServer;
	CTestDlg * m_Dlg;
	// _IPluginEvents Methods
public:

	// IPlugin Methods
public:
	STDMETHOD(GetFunctionCount)( SHORT *  pCount)
	{
		// 在此处添加函数实现。
		*pCount=FUNCTION_NUM;
		return S_OK;
	}
	STDMETHOD(DoFunction)( SHORT  index)
	{
		//----------------------------------------
		switch(index)
		{
		case 0:
			//AfxMessageBox("I am Plugin's first function");
			::MessageBox(NULL,"无模式弹出窗口信息条","提示信息",MB_OK);
			break;
		case 1:
			AfxMessageBox("有模式窗口信息弹出提示");
			break;
		case 2:
			if(m_Dlg==NULL)
			{
				m_Dlg=new CTestDlg();	
				m_Dlg->Create(NULL);
			}
			m_Dlg->ShowWindow(SW_SHOW);
			break;
		//default:;

		}
		
		return S_OK;
	}
	STDMETHOD(GetFunctionName)( SHORT  index,  CHAR *  name)
	{
		// 在此处添加函数实现。
		// TODO: 在此添加实现代码
		switch(index)
		{
		case 0:
			strcpy(name,"简单功能1");
			break;
		case 1:
			strcpy(name,"简单功能2");
			break;
		case 2:
			strcpy(name,"高级功能");
			break;
		default:;

		}
		return S_OK;
	}
	STDMETHOD(GetPluginName)(CHAR * name)
	{
		// 在此处添加函数实现。
		strcpy(name,"测试插件");
		return S_OK;
	}
	
	STDMETHOD(SendMouseMessage)(UINT Message, UINT flags, int x, int y)
	{
		return S_OK;
	}
	STDMETHOD(PassHDC)(HDC hDC)
	{
		return S_OK;
	};

	STDMETHOD(PassHWND)(HWND mHWND)
	{
		return S_OK;
	};
	STDMETHOD(SetServer)(IUnknown* iServer)
	{
		return S_OK;
	};
	STDMETHOD(ReDraw)(void)
	{
		return S_OK;
	};
	STDMETHOD(GetToolBarBitmap)(HBITMAP* hBitmap)
	{
		*hBitmap=::LoadBitmap(global_hInstance,MAKEINTRESOURCE(IDB_TOOLBAR));
		return S_OK;
	};
};

OBJECT_ENTRY_AUTO(__uuidof(L1Plugin), CL1Plugin)
