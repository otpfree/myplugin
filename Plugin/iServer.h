
//--------------------------------------------------
//iserver.h
#include "commstruct.h"
interface DECLSPEC_UUID("AF4258EC-938B-475B-A55F-4167B11F20A7")
IServer : public IUnknown
{
	virtual int /*  [helpstring] */ STDMETHODCALLTYPE GetDataPoint(myGraph* dPointer)=0;
	virtual int /*[helpstring] */ STDMETHODCALLTYPE NewDataCircle(myGraph* pGraph)=0;
};
