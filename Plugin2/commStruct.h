//commstruct.h
//--------------------------------------
struct _myGraph
{
	int mWidth;		//线的宽度
	unsigned long mRed,mGreen,mBlue;//线的颜色
	long x, y;	    //对于圆来说，为圆心点
	int mRate;		//{1,2,3}三种比率
	struct _myGraph * next;  //链接指针
};
typedef struct _myGraph  myGraph;
