// MyDialog.h : CMyDialog 的声明

#pragma once

#include "resource.h"       // 主符号
#include <atlhost.h>
#include "commstruct.h"
#include "iServer.h"
// CMyDialog

class CMyDialog : 
	public CAxDialogImpl<CMyDialog>
{
public:
	CMyDialog()
		: m_Rate(1)
		, m_Width(1)
		, m_Red(255)
		, m_Green(0)
		, m_Blue(0)
	{

	}
	CMyDialog(HDC hDC)
		: m_Rate(1)
		, m_Width(1)
		, m_Red(255)
		, m_Green(0)
		, m_Blue(0)
	{
		m_hDC=hDC;
		m_bPaint=false;
		 m_bCircle=false;
		 m_bDraw=false;

	}

	~CMyDialog()
	{
		//delete m_Graph;
	}

	HDC m_hDC;
	BOOL m_bPaint;
	myGraph* m_Graph;
	IServer* m_dlgServer;
	BOOL m_bCircle,m_bDraw;
	enum { IDD = IDD_MYDIALOG };

BEGIN_MSG_MAP(CMyDialog)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
	COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
	COMMAND_HANDLER(IDC_BUTTON1, BN_CLICKED, OnBnClickedButton1)
	COMMAND_HANDLER(IDC_BUTTON2, BN_CLICKED, OnBnClickedButton2)
	COMMAND_HANDLER(IDC_CHKPAINT, BN_CLICKED, OnBnClickedChkpaint)
	COMMAND_HANDLER(IDC_BUTCIRCLE, BN_CLICKED, OnBnClickedButcircle)
	COMMAND_HANDLER(IDC_CHECK2, BN_CLICKED, OnBnClickedCheck2)
	COMMAND_HANDLER(IDC_CHECK3, BN_CLICKED, OnBnClickedCheck3)
	COMMAND_HANDLER(IDC_EDITRATE, EN_CHANGE, OnEnChangeEditrate)
	COMMAND_HANDLER(IDC_EDITWIDTH, EN_CHANGE, OnEnChangeEditwidth)
	COMMAND_HANDLER(IDC_RED, BN_CLICKED, OnBnClickedRed)
	COMMAND_HANDLER(IDC_BLUE, BN_CLICKED, OnBnClickedRed)
	COMMAND_HANDLER(IDC_GREEN, BN_CLICKED, OnBnClickedRed)
	CHAIN_MSG_MAP(CAxDialogImpl<CMyDialog>)
END_MSG_MAP()

// 处理程序原型：
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		CAxDialogImpl<CMyDialog>::OnInitDialog(uMsg, wParam, lParam, bHandled);
		
		::SetDlgItemInt(this->m_hWnd,IDC_EDITRATE,1,false);
		::SetDlgItemInt(this->m_hWnd,IDC_EDITWIDTH,1,false);
		::CheckRadioButton(this->m_hWnd,IDC_RED,IDC_BLUE,IDC_RED);
 
		return 1;  // 使系统设置焦点
	}

	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		//EndDialog(wID);
		ShowWindow(SW_HIDE);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		//EndDialog(wID);
		ShowWindow(SW_HIDE);
		return 0;
	}
	LRESULT OnTcnSelchangeTab1(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/);
	LRESULT OnBnClickedButton1(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnBnClickedButton2(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	void DrawTo(POINT Source, POINT Targe);
	LRESULT OnBnClickedChkpaint(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnBnClickedButcircle(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnBnClickedCheck2(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	
	LRESULT OnBnClickedCheck3(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnEnChangeEditrate(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	int m_Rate;
	int m_Width;
	unsigned long m_Red;
	unsigned long m_Green;
	unsigned long m_Blue;
	LRESULT OnEnChangeEditwidth(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnBnClickedRed(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	//LRESULT OnBnClickedBlue(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	//LRESULT OnBnClickedGreen(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
};


