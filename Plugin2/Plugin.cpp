// Plugin.cpp : CPlugin 的实现

#include "stdafx.h"
#include <string.h>
#include "Plugin.h"

//#include <Winuser.h>
//#include <windows.h>

// CPlugin
extern HINSTANCE global_hInstance; //全局程序实例句柄

STDMETHODIMP CPlugin::GetFunctionCount(SHORT* pCount)
{
	// TODO: 在此添加实现代码
	*pCount=FUNCTION_NUM;
	return S_OK;
}

STDMETHODIMP CPlugin::DoFunction(SHORT index)
{
	//----------------------------------------
	myGraph* mGraph=NULL;
	switch(index)
	{
	case 0:
		//MessageBox(NULL,"I am Plugin2's first function","Hello",MB_OK);
		if (m_myDlg==NULL)
		{
			m_myDlg=new CMyDialog(m_hDC);
			m_myDlg->Create(NULL);
			
			
		}//delete m_myDlg;
		m_pServer->GetDataPoint(&mGraph);
		m_myDlg->m_Graph=mGraph;
		m_myDlg->m_dlgServer=m_pServer;
		//m_pGraph=mGraph;
		m_myDlg->ShowWindow(SW_SHOWNORMAL);
		m_myDlg->BringWindowToTop();
		break;
	case 1:
		//MessageBox(NULL,"简单的信息提示!","插件提示",MB_OK);
		CTestDialog mDlg;
		mDlg.m_hWND=m_hWND;
		mDlg.DoModal();
		
		break;
	
	//default:;
	}
	
	return S_OK;
}

STDMETHODIMP CPlugin::GetFunctionName(SHORT index, CHAR* name)
{
	// TODO: 在此添加实现代码
	switch(index)
	{
	case 0:
		strcpy(name,"Scribble");
		break;
	case 1:
		strcpy(name,"功能调用");
		break;
	default:
			;
	}
	return S_OK;
}

STDMETHODIMP CPlugin::GetPluginName(CHAR* name)
{
	// TODO: 在此添加实现代码
	strcpy(name,"图形插件");
	return S_OK;
}

STDMETHODIMP CPlugin::SendMouseMessage(UINT Message, UINT flags, int x, int y)
{
	// TODO: 在此添加实现代码
	if (m_myDlg!=NULL)
	{
		HWND hwndStatus =m_myDlg->GetDlgItem(IDC_EDIT1);
		TCHAR szCookieItem[100];
		switch(Message)
		{
		case WM_MOUSEMOVE:
			_stprintf(szCookieItem, _T("MouseMove( x position %d,y position%d) "), x,y);
			if (m_Paint){
				m_Target.x=x;m_Target.y=y;
				m_myDlg->DrawTo(m_Source,m_Target);
				m_Source.x=m_Target.x;m_Source.y=m_Target.y;
			}
			if ((m_myDlg->m_bPaint) && m_Paint)
			///*if (m_Paint)
			{
				::SetCursor(::LoadCursor(NULL,IDC_CROSS));
			}else
			{
				::SetCursor(::LoadCursor(NULL,IDC_ARROW));
			}

			break;
		case WM_LBUTTONDOWN:		
			_stprintf(szCookieItem, _T("MouseLeftbuttondown( x position %d,y position%d) "), x,y);
			m_Source.x=x;m_Source.y=y;
			m_Paint=true;
			if ((m_myDlg->m_bPaint) && m_Paint)
				::SetCursor(::LoadCursor(NULL,IDC_CROSS));
			break;
		case WM_LBUTTONUP:		
			_stprintf(szCookieItem, _T("MouseLeftbuttonup( x position %d,y position%d) "), x,y);
			m_Paint=false;
			//if (m_myDlg->m_bPaint)
			::SetCursor(::LoadCursor(NULL,IDC_ARROW));
			break;
		case WM_LBUTTONDBLCLK:		
			_stprintf(szCookieItem, _T("MouseDoublebuttonclick( x position %d,y position%d) "), x,y);
			m_Paint=false;
			//-------------画圆------------
			if (m_myDlg->m_bCircle)
			{	
				::SetCursor(::LoadCursor(NULL,IDC_UPARROW));
				HPEN hPen = ::CreatePen(PS_SOLID,m_myDlg->m_Width,RGB(m_myDlg->m_Red,m_myDlg->m_Green,m_myDlg->m_Blue) ) ;
				HPEN hOldPen = (HPEN) ::SelectObject(m_hDC, hPen) ;
				::Ellipse(m_hDC,x-20*m_myDlg->m_Rate, y-20*m_myDlg->m_Rate,
					x+20*m_myDlg->m_Rate,y+20*m_myDlg->m_Rate);
				::SelectObject(m_hDC,hOldPen);
				LOGBRUSH logBrush;
				logBrush.lbStyle = BS_NULL;
				logBrush.lbColor = RGB(0, 192, 192);
				logBrush.lbHatch = HS_CROSS;
				HBRUSH brush=::CreateBrushIndirect(&logBrush);
				::SelectObject(m_hDC,brush);

				myGraph mGraph;
				mGraph.mRed=m_myDlg->m_Red;mGraph.mGreen=m_myDlg->m_Green;mGraph.mBlue=m_myDlg->m_Blue;
				mGraph.mRate=m_myDlg->m_Rate;mGraph.x=x;mGraph.y=y;mGraph.mWidth=m_myDlg->m_Width;
				mGraph.next=NULL;
				m_pServer->NewDataCircle(&mGraph);

			}
			::SetCursor(::LoadCursor(NULL,IDC_ARROW));

			//-----------------------------
			break;
		default:
			_stprintf(szCookieItem, _T("OtherMouseMessages( x position %d,y position%d) "), x,y);
			break;


		
		}
		
		::SetWindowText(hwndStatus, szCookieItem);

	}
	return S_OK;
}

STDMETHODIMP CPlugin::PassHDC(HDC hDC)
{
	// TODO: 在此添加实现代码
	m_hDC=hDC;

	return S_OK;
}

STDMETHODIMP CPlugin::PassHWND(HWND mHWND)
{
	// TODO: 在此添加实现代码
	m_hWND=mHWND;

	return S_OK;
}

STDMETHODIMP CPlugin::SetServer(IUnknown* iServer)
{
	// TODO: 在此添加实现代码
	m_pServer=(IServer*)iServer;
	return S_OK;
}

STDMETHODIMP CPlugin::ReDraw(void)
{
	//-------------------------------------------
	if (m_myDlg==NULL) return S_OK;
	if (!(m_myDlg->m_bDraw)) return S_OK;
	m_pServer->GetDataPoint(&m_pGraph);
	myGraph *mGraph;
	mGraph=m_pGraph;

	while (mGraph!=NULL)
	{	//创建为空刷子，以使圆透明
		LOGBRUSH logBrush;
		logBrush.lbStyle = BS_NULL;
		logBrush.lbColor = RGB(0, 192, 192);
		logBrush.lbHatch = HS_CROSS;
		HBRUSH brush=::CreateBrushIndirect(&logBrush);
		//::SelectObject(m_hDC,brush);
		HBRUSH oldBrush=(HBRUSH)::SelectObject(m_hDC,brush);
		
		HPEN hPen = ::CreatePen(PS_SOLID,mGraph->mWidth,RGB(mGraph->mRed,mGraph->mGreen,mGraph->mBlue) ) ;
		HPEN hOldPen = (HPEN) ::SelectObject(m_hDC, hPen) ;
		::Ellipse(m_hDC,mGraph->x-20*mGraph->mRate, mGraph->y-20*mGraph->mRate,
					mGraph->x+20*mGraph->mRate,mGraph->y+20*mGraph->mRate);
		::SelectObject(m_hDC,hOldPen);
		::SelectObject(m_hDC,oldBrush);
			
		
		mGraph=mGraph->next;
		
	}

	return S_OK;
}

STDMETHODIMP CPlugin::GetToolBarBitmap(HBITMAP* hBitmap)
{
	// TODO: 在此添加实现代码
	*hBitmap=::LoadBitmap(global_hInstance,MAKEINTRESOURCE(IDB_PALETTE));
	return S_OK;
}
