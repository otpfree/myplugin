// MyDialog.cpp : CMyDialog 的实现

#include "stdafx.h"
#include "MyDialog.h"


// CMyDialog

LRESULT CMyDialog::OnBnClickedButton1(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	// TODO: 在此添加控件通知处理程序代码
	// Get a device context to draw on from the canvas.
		// Make a pen.
	BOOL m_bSelected=true;
	HPEN hPen = ::CreatePen(PS_SOLID, 6, m_bSelected ? RGB(255,250,0) : RGB(128,128,128)) ;
	HPEN hOldPen = (HPEN) ::SelectObject(m_hDC, hPen) ;

	// Make a brush.
	HBRUSH hOldBrush = (HBRUSH)::SelectObject(m_hDC, ::GetStockObject(m_bSelected ? NULL_BRUSH : LTGRAY_BRUSH)) ;

	// Draw the polygon.
	POINT pts[4];
	pts[0].x = 20;
	pts[0].y = 20;

	pts[1].x = 20;
	pts[1].y = 140;

	pts[2].x = 50;
	pts[2].y = 40;

	pts[3].x = 50;
	pts[3].y = 60;

	//::Polygon(hDC, m_ptVertices, m_cVertices) ;
	::Polygon(m_hDC,pts,4);

	// CleanUp
	::SelectObject(m_hDC, hOldPen) ;
	::SelectObject(m_hDC, hOldBrush) ;

	::DeleteObject(hPen) ;
	return 0;
}

LRESULT CMyDialog::OnBnClickedButton2(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	// TODO: 在此添加控件通知处理程序代码
	// Get a device context to draw on from the canvas.
		// Make a pen.
	BOOL m_bSelected=true;
	HPEN hPen = ::CreatePen(PS_SOLID, 5, m_bSelected ? RGB(255,0,255) : RGB(128,128,128)) ;
	HPEN hOldPen = (HPEN) ::SelectObject(m_hDC, hPen) ;

	// Make a brush.
	HBRUSH hOldBrush = (HBRUSH)::SelectObject(m_hDC, ::GetStockObject(m_bSelected ? NULL_BRUSH : LTGRAY_BRUSH)) ;

	// Draw the polygon.
	POINT pts[4];
	pts[0].x = 100;
	pts[0].y = 220;

	pts[1].x = 220;
	pts[1].y = 140;

	pts[2].x = 150;
	pts[2].y = 140;

	pts[3].x = 150;
	pts[3].y = 160;

	//::Polygon(hDC, m_ptVertices, m_cVertices) ;
	::Polygon(m_hDC,pts,4);

	// CleanUp
	::SelectObject(m_hDC, hOldPen) ;
	::SelectObject(m_hDC, hOldBrush) ;

	::DeleteObject(hPen) ;
	return 0;

	return 0;
}

void CMyDialog::DrawTo(POINT Source, POINT Targe)
{
	if (!m_bPaint) return;
	HPEN hPen = ::CreatePen(PS_SOLID, 6,RGB(255,0,0) ) ;
	HPEN hOldPen = (HPEN) ::SelectObject(m_hDC, hPen) ;

	// Make a brush.
	HBRUSH hOldBrush = (HBRUSH)::SelectObject(m_hDC, ::GetStockObject( NULL_BRUSH)) ;
	
	//::LineTo(m_hDC,Targe.x,Targe.y);
	//::Polygon(m_hDC,pts,4);
	POINT pts[2];
	pts[0].x=Source.x;
	pts[0].y=Source.y;
	pts[1].x=Targe.x;
	pts[1].y=Targe.y;
	::Polyline(m_hDC,pts,2);
	//// CleanUp
	::SelectObject(m_hDC, hOldPen) ;
	::SelectObject(m_hDC, hOldBrush) ;

	::DeleteObject(hPen) ;
	//return 0;
}

LRESULT CMyDialog::OnBnClickedChkpaint(WORD /*wNotifyCode*/, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	// TODO: 在此添加控件通知处理程序代码
	//	HWND hwndStatus =GetDlgItem(IDC_EDIT1);
	m_bPaint=::IsDlgButtonChecked(this->m_hWnd,wID);
	//m_bPaint=IsDlgButtonChecked(IDC_EDIT1);
	return 0;
}

LRESULT CMyDialog::OnBnClickedButcircle(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	myGraph *mGraph;
	m_dlgServer->GetDataPoint(&m_Graph);
	mGraph=m_Graph;
	while (mGraph!=NULL)
	{
		HPEN hPen = ::CreatePen(PS_SOLID,mGraph->mWidth,RGB(mGraph->mRed,mGraph->mGreen,mGraph->mBlue) ) ;
		HPEN hOldPen = (HPEN) ::SelectObject(m_hDC, hPen) ;
		::Ellipse(m_hDC,mGraph->x-20*mGraph->mRate, mGraph->y-20*mGraph->mRate,
					mGraph->x+20*mGraph->mRate,mGraph->y+20*mGraph->mRate);
		::SelectObject(m_hDC,hOldPen);
		mGraph=mGraph->next;
		
	}


	return 0;
}

LRESULT CMyDialog::OnBnClickedCheck2(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
		//	HWND hwndStatus =GetDlgItem(IDC_EDIT1);
	m_bCircle=::IsDlgButtonChecked(this->m_hWnd,wID);
	//m_bPaint=IsDlgButtonChecked(IDC_EDIT1);

	return 0;
}

LRESULT CMyDialog::OnBnClickedCheck3(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	// TODO: 在此添加控件通知处理程序代码
	//	HWND hwndStatus =GetDlgItem(IDC_EDIT1);
	//m_bPaint=IsDlgButtonChecked(IDC_EDIT1);
	m_bDraw=::IsDlgButtonChecked(this->m_hWnd,wID);

	return 0;
}

LRESULT CMyDialog::OnEnChangeEditrate(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	// TODO:  在此添加控件通知处理程序代码
	int		iRate;
	BOOL	fError;
	iRate = ::GetDlgItemInt(this->m_hWnd, wID, 
                        &fError, false); 
	if (fError)
	{
		m_Rate=iRate;	
	}
	else
	{
		::SetDlgItemInt(this->m_hWnd,wID,m_Rate,false);
	}

	return 0;
}

LRESULT CMyDialog::OnEnChangeEditwidth(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	// TODO:  在此添加控件通知处理程序代码
	int		iRate;
	BOOL	fError;
	iRate = ::GetDlgItemInt(this->m_hWnd, wID, 
                        &fError, false); 
	if (fError)
	{
		m_Width=iRate;	
	}
	else
	{
		::SetDlgItemInt(this->m_hWnd,wID,m_Width,false);
	}
	return 0;
}

LRESULT CMyDialog::OnBnClickedRed(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	// TODO: 在此添加控件通知处理程序代码
	if(wID==IDC_RED)  {m_Red=255; m_Green=0;   m_Blue=0;}
	if(wID==IDC_GREEN){m_Red=0;   m_Green=255; m_Blue=0;}
	if(wID==IDC_BLUE) {m_Red=0;   m_Green=0;   m_Blue=255;}
	return 0;
}


