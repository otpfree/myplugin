// TestDialog.h : CTestDialog 的声明

#pragma once

#include "resource.h"       // 主符号
#include <atlhost.h>


// CTestDialog

class CTestDialog : 
	public CAxDialogImpl<CTestDialog>
{
public:
	CTestDialog()
	{
	}

	~CTestDialog()
	{
	}
	HWND m_hWND;
	enum { IDD = IDD_TESTDIALOG };

BEGIN_MSG_MAP(CTestDialog)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
	COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
	COMMAND_HANDLER(IDC_BUTMODIFY, BN_CLICKED, OnBnClickedButmodify)
	//COMMAND_HANDLER(IDC_BUTREAD, BN_CLICKED, OnBnClickedButread)
	CHAIN_MSG_MAP(CAxDialogImpl<CTestDialog>)
END_MSG_MAP()

// 处理程序原型：
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		CAxDialogImpl<CTestDialog>::OnInitDialog(uMsg, wParam, lParam, bHandled);
		return 1;  // 使系统设置焦点
	}

	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}
	LRESULT OnBnClickedButmodify(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	
};


