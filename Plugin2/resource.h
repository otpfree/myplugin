//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Plugin2.rc
//
#define IDS_PROJNAME                    100
#define IDR_PLUGIN2                     101
#define IDR_PLUGIN                      102
#define IDD_MYDIALOG                    103
#define IDD_TESTDIALOG                  104
#define IDC_EDIT1                       201
#define IDC_LIST1                       202
#define IDC_LISTPOINT                   202
#define IDB_STYLE                       202
#define IDC_BUTTON1                     203
#define IDB_BITMAP3                     203
#define IDB_PALETTE                     203
#define IDC_BUTTON2                     204
#define IDC_TAB1                        205
#define IDC_CHECK1                      206
#define IDC_CHKPAINT                    206
#define IDC_RADIO1                      207
#define IDC_RADIO2                      208
#define IDC_RADIO3                      209
#define IDC_CHECK2                      213
#define IDC_BUTMODIFY                   214
#define IDC_CHECK3                      214
#define IDC_BUTMODIFY2                  215
#define IDC_BUTREAD                     215
#define IDC_BUTCIRCLE                   215
#define IDC_EDITRATE                    217
#define IDC_SPIN2                       218
#define IDC_EDITWIDTH                   219
#define IDC_RED                         221
#define IDC_GREEN                       222
#define IDC_BLUE                        223

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        204
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         224
#define _APS_NEXT_SYMED_VALUE           105
#endif
#endif
