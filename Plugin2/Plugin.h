// Plugin.h : CPlugin 的声明

#pragma once
#include "resource.h"       // 主符号

//--------------------------------------
#include "MyDialog.h"
#include "TestDialog.h"
#include "Plugin2.h"
#include "_IPluginEvents_CP.h"
//#include "iserver.h"

//extern HINSTANCE global_hInstance; //全局程序实例句柄

// CPlugin
const short  FUNCTION_NUM = 2;


class ATL_NO_VTABLE CPlugin : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CPlugin, &CLSID_Plugin>,
	public IConnectionPointContainerImpl<CPlugin>,
	public CProxy_IPluginEvents<CPlugin>, 
	public IPlugin
{
public:
	CPlugin()
	{
		//m_myDlg=new CMyDialog();
		m_myDlg=NULL;
		m_Paint=false;
	}
	~CPlugin()
	{
		if (m_myDlg!=NULL) delete m_myDlg;
		m_Paint=false;
	}

DECLARE_REGISTRY_RESOURCEID(IDR_PLUGIN)


BEGIN_COM_MAP(CPlugin)
	COM_INTERFACE_ENTRY(IPlugin)
	COM_INTERFACE_ENTRY(IConnectionPointContainer)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(CPlugin)
	CONNECTION_POINT_ENTRY(__uuidof(_IPluginEvents))
END_CONNECTION_POINT_MAP()

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

public:

	STDMETHOD(GetFunctionCount)(SHORT* pCount);
	STDMETHOD(DoFunction)(SHORT index);
	STDMETHOD(GetFunctionName)(SHORT index, CHAR* name);
	STDMETHOD(GetPluginName)(CHAR* name);
public:
	CMyDialog*  m_myDlg;
	HDC m_hDC;
	HWND m_hWND;
	BOOL m_Paint;
	POINT m_Source,m_Target;
	IServer* m_pServer;
	myGraph* m_pGraph;

	STDMETHOD(SendMouseMessage)(UINT Message, UINT flags, int x, int y);
	STDMETHOD(PassHDC)(HDC hDC);
	STDMETHOD(PassHWND)(HWND mHWND);
	STDMETHOD(SetServer)(IUnknown* iServer);
	STDMETHOD(ReDraw)(void);
	STDMETHOD(GetToolBarBitmap)(HBITMAP* hBitmap);
};

OBJECT_ENTRY_AUTO(__uuidof(Plugin), CPlugin)
